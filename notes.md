# Async python

## Part1

Part1 of this (generators and friends): https://gitlab.com/trimailov/be_guild_async_python

## Reminder about generator and generator coroutines

- Generators stop their execution on `yield` expression and return control to
  the calling function;
- `yield` can both return or receive a value;
- Calling generator function will return generator object, to actually run and
  step through generator, use `next(generator)` function or `generator.send()`
  method;
- `yield from` - loops and yields from another generator (or iterable);
- These concepts allow us to implement an event-loop based concurrency model
  (cooperative multitasking). Because `yield` stops execution of the generator
  and returns control to the caller - we can context switch between different
  functions and improve performance for IO bound tasks.

## History of concurrency in python

This is not in some particular order, but before asyncio, there were multiple
projects for having concurrency (or async/await) primitives in python.

- greenlets/gevent
- stackless python
- twisted
- tornado
- curio

## Async/await

`async def` - creates a _coroutine_.

Just like _generator coroutines_ (python functions that use `yield` instead of
`return`). It is not executed on direct call, we need to use explicit syntax to
actually execute (iterate?) the coroutine.

`await` - iterates the coroutine (equivalent to `yield from`). `await` tells
the `asyncio` - that it is save to context switch [2].

## Generator coroutines vs async/await

```
Generators                        | asyncio
----------------------------------+-----------------------------------
yield instead of return           | async def
----------------------------------+-----------------------------------
yield from                        | await
----------------------------------+-----------------------------------
simple call returns generator obj | simple call returns coroutine obj
----------------------------------+-----------------------------------
no event loop                     | event loop and friends included
----------------------------------+-----------------------------------
```

NB: support for generator coroutines in asyncio (async/await) is deprecated.

## asyncio [3]

https://docs.python.org/3/library/asyncio-api-index.html

Awaitables - objects that can be awaited. Main 3 are: coroutines (`async def`), Tasks, Futures.

`asyncio.run()` - runs a coroutine and returns the result.

`asyncio.create_task()` - wraps coroutine into a task and schedules its execution.

`asyncio.gather()` - run awaitables concurrently.

`async with` - context managers that are async, e.g. establish async socket connection.

`async for` - iterates over asynchronous iterable, e.g. read data stream as
soon as data is received, and not when everything is received.

`asyncio.get_event_loop().run_in_executor()` - will spawn a thread to run non async code.

### Tip

Seperate CPU intensive code (non-async) from IO bound implementation (async) [2].

## Summary

- Asyncio needs event loop;
- Obvious gains in IO bound applications (e.g. webservers);
- Application controls its own context switching;
- Single threaded - so no worries about locks and other thread pitfalls;
- There are 2 python worlds - sync vs async and they don't usually mix;
- Cool stuff like [FastAPI](https://fastapi.tiangolo.com/);

## Resources

[0] https://www.youtube.com/watch?v=ReXxO_azV-w - Yury Selivanov - Asyncio in Python 3 7 and 3 8
[1] https://www.youtube.com/watch?v=i0RB7UqxERE - Async await in Nim A demonstration of the flexibility metaprogramming can bring to a language
[2] https://www.youtube.com/watch?v=bs9tlDFWWdQ - Asyncio: Understanding Async / Await in Python
[3] https://docs.python.org/3/library/asyncio-task.html
[4] https://www.youtube.com/watch?v=GSiZkP7cI80 - "What is a Coroutine Anyway?" - John Reese (North Bay Python 2019)
[4.1] https://github.com/jreese/pycon/tree/master/what-is-a-coroutine
