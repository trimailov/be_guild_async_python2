import asyncio
import time


async def say_after(delay, what):
    await asyncio.sleep(delay)
    print(what)


async def main():

    task1 = asyncio.create_task(say_after(1, 'hello'))
    task2 = asyncio.create_task(say_after(2, 'world'))

    print(f"started at {time.strftime('%X')}")

    await task1
    # Wait until both tasks are completed (should take around 2 seconds.)
    print(f"middle at {time.strftime('%X')}")
    await task2

    print(f"finished at {time.strftime('%X')}")


# 12,13 line - initiate tasks in our event loop
# 18 line - wait for task1 (~1s)
# 20 line - wait for task2 (for ~1s more, or ~2seconds in total)


asyncio.run(main())
