import asyncio
import time

async def handle_echo(reader, writer):
    addr = writer.get_extra_info('peername')

    print(f"Received connection from {addr!r}")

    data = await reader.read(100)
    await asyncio.sleep(10)
    print(f"finished sleeping {time.strftime('%X')}")
    message = data.decode()

    print(f"Received {message!r} from {addr!r}")

    print(f"Send: {message!r}")
    writer.write(data)
    await writer.drain()

    print("Close the connection")
    writer.close()


# listens to data on localhost port 8888
# to connect use `telnet localhost 8888`
async def main():
    server = await asyncio.start_server(handle_echo, '127.0.0.1', 8888)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')

    async with server:
        await server.serve_forever()

asyncio.run(main())
